//
//  ImportSuccessfullyViewController.swift
//  Story Uploader
//
//  Created by User543 on 14.07.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ImportSuccessfullyViewController: UIViewController, GADBannerViewDelegate  {
    
    
    @IBOutlet weak var gadBannerView: GADBannerView!
    
    @IBAction func goToInstagramStory(_ sender: UIButton) {
        self.goToInstagram()
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID]
        // MARK: Google Ads Key
        gadBannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        gadBannerView.rootViewController = self
        gadBannerView.delegate = self
        gadBannerView.load(GADRequest())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func goToInstagram() {
        let instagramHooks = "instagram://app"
        let instagramUrl = URL.init(string: instagramHooks)!
        if UIApplication.shared.canOpenURL(instagramUrl as URL)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(instagramUrl, options: [:])
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(instagramUrl)
            }
        } else {
            let message = "Please install Instagram App first from App Store."
            let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                if let url = URL(string: "itms-apps://search.itunes.apple.com/WebObjects/MZSearch.woa/wa/search?media=software&term=instagram") {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:])
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            })
            alert.addAction(action)
            let topVC = self.getCurrentViewController(self)!
            topVC.present(alert, animated: true, completion: nil)
        }
    }
}
