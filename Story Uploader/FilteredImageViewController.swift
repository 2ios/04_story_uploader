//
//  FilteredImageViewController.swift
//  Story Uploader
//
//  Created by User543 on 26.07.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import MobileCoreServices
import KeychainSwift

class FilteredImageViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    
    // Outlet & action - save button
    @IBOutlet var saveButton: UIBarButtonItem!
    @IBAction func saveButtonAction(_ sender: UIBarButtonItem) {
        // save image to photo gallery
        var result = UserDefaults.standard.integer(forKey: "Wallet")
        let alert = UIAlertController(title: "Attention!",
                                      message:"It will cost you \(300) coins",
            preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            result -= 300
            UserDefaults.standard.set(result, forKey: "Wallet")
            UserDefaults.standard.synchronize()
            let keychain = KeychainSwift()
            print(String(result))
            keychain.set(String(result), forKey: "Wallet")
            keychain.synchronizable = true
            print(keychain.get("Wallet")!)
            self.saveImageToPhotoGallery()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func cancelButton(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    // Outlet - image preview
    @IBOutlet var previewImageView: UIImageView!
    
    // Selected image
    var selectedImage: UIImage!
    
    // filter Title and Name list
    var filterTitleList: [String]!
    var filterNameList: [String]!
    
    // filter selection picker
    @IBOutlet var filterPicker: UIPickerView!
    
    // MARK: - view functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // set filter title list array.
        self.filterTitleList = ["None" ,"EffectChrome", "EffectFade", "EffectInstant", "EffectMono", "EffectNoir", "EffectProcess", "EffectTonal", "EffectTransfer", "EffectSepiaTone", "EffectVignette", "EffectInvert", "EffectMonochrome", "EffectPosterize", "EffectFalseColor", "EffectMaximum", "EffectMinimum"]
        
        // set filter name list array.
        self.filterNameList = ["No Filter" ,"CIPhotoEffectChrome", "CIPhotoEffectFade", "CIPhotoEffectInstant", "CIPhotoEffectMono", "CIPhotoEffectNoir", "CIPhotoEffectProcess", "CIPhotoEffectTonal", "CIPhotoEffectTransfer","CISepiaTone", "CIVignette" , "CIColorInvert", "CIColorMonochrome", "CIColorPosterize", "CIFalseColor", "CIMaximumComponent", "CIMinimumComponent"]
        
        // set delegate for filter picker
        self.filterPicker.delegate = self
        self.filterPicker.dataSource = self
        
        // enable filter pickerView
        self.filterPicker.isUserInteractionEnabled = true
        self.filterPicker.selectRow(0, inComponent: 0, animated: true)
        
        // disable save button
        self.saveButton.isEnabled = false
        
        // present preview image
        self.previewImageView.image = selectedImage
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - picker view delegate and data source (to choose filter name)
    
    // how many component (i.e. column) to be displayed within picker view
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // How many rows are there is each component
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.filterTitleList.count
    }
    
    // title/content for row in given component
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.filterTitleList[row]
    }
    
    // called when row selected from any component within picker view
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        // disable save button if filter not selected.
        // enable save button if filter selected.
        if row == 0 {
            self.saveButton.isEnabled = false
        }else{
            self.saveButton.isEnabled = true
        }
        // call funtion to apply the selected filter
        self.applyFilter(selectedFilterIndex: row)
    }
    
    // apply filter to current image
    fileprivate func applyFilter(selectedFilterIndex filterIndex: Int) {
        
        //        print("Filter - \(self.filterNameList[filterIndex)")
        
        /* filter name
         0 - NO Filter,
         1 - PhotoEffectChrome, 2 - PhotoEffectFade, 3 - PhotoEffectInstant, 4 - PhotoEffectMono,
         5 - PhotoEffectNoir, 6 - PhotoEffectProcess, 7 - PhotoEffectTonal, 8 - PhotoEffectTransfer
         */
        
        // if No filter selected then apply default image and return.
        if filterIndex == 0 {
            // set image selected image
            self.previewImageView.image = self.selectedImage
            return
        }
        
        
        // Create and apply filter
        // 1 - create source image
        let sourceImage = CIImage(image: self.selectedImage)
        
        // 2 - create filter using name
        let myFilter = CIFilter(name: self.filterNameList[filterIndex])
        myFilter?.setDefaults()
        
        // 3 - set source image
        myFilter?.setValue(sourceImage, forKey: kCIInputImageKey)
        
        // 4 - create core image context
        let context = CIContext(options: nil)
        
        // 5 - output filtered image as cgImage with dimension.
        let outputCGImage = context.createCGImage(myFilter!.outputImage!, from: myFilter!.outputImage!.extent)
        
        // 6 - convert filtered CGImage to UIImage
        let filteredImage = UIImage(cgImage: outputCGImage!)
        
        // 7 - set filtered image to preview
        self.previewImageView.image = filteredImage
    }
    
    
    // save imaage to photo gallery
    fileprivate func saveImageToPhotoGallery(){
        // Save image
        DispatchQueue.main.async {
            UIImageWriteToSavedPhotosAlbum(self.previewImageView.image!, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
        }
    }
    
    // show message after image saved to photo gallery.
    func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        
        // show success or error message.
        if error == nil {
            self.showAlertMessage(alertTitle: "Success", alertMessage: "Image Saved To Photo Gallery")
        } else {
            self.showAlertMessage(alertTitle: "Error!", alertMessage: (error?.localizedDescription)! )
        }
        
    }
    
    // Show alert message with OK button
    func showAlertMessage(alertTitle: String, alertMessage: String) {
        
        let myAlertVC = UIAlertController( title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            self.goToImportSuccessfullyViewController()
        })
        myAlertVC.addAction(okAction)
        self.present(myAlertVC, animated: true, completion: nil)
    }
    
    func goToImportSuccessfullyViewController() {
        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //        let controller = storyboard.instantiateViewController(withIdentifier: "ImportSuccessfullyViewController")
        //        controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        //        self.addChildViewController(controller)
        //        self.view.addSubview(controller.view)
        
        let alert = UIAlertController(title: "Congratulations!",
                                      message:"Import successfully",
                                      preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Back", style: UIAlertActionStyle.default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Go to Instagram", style: UIAlertActionStyle.cancel, handler: { (action) in
            self.goToInstagram()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func goToInstagram() {
        let instagramHooks = "instagram://app"
        let instagramUrl = URL.init(string: instagramHooks)!
        if UIApplication.shared.canOpenURL(instagramUrl as URL)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(instagramUrl, options: [:])
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(instagramUrl)
            }
        } else {
            let message = "Please install Instagram App first from App Store."
            let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                if let url = URL(string: "itms-apps://search.itunes.apple.com/WebObjects/MZSearch.woa/wa/search?media=software&term=instagram") {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:])
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            })
            alert.addAction(action)
            let topVC = self.getCurrentViewController(self)!
            topVC.present(alert, animated: true, completion: nil)
        }
    }
}


