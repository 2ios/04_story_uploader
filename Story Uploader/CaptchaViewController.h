//
//  CaptchaViewController.h
//  LikeHelper
//
//  Created by Motipan Serj on 12/16/16.
//  Copyright © 2016 Piotr Arabadji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CaptchaViewController : UIViewController<UIWebViewDelegate,NSURLSessionDelegate>
@property (strong, nonatomic) NSString *link;
@property (assign, nonatomic) BOOL isHiddenTop;
@property (strong, nonatomic) NSString *cookies;
@property (assign, nonatomic) NSMutableData *receivedData;
//@property (strong, nonatomic) NSMutableURLRequest *requestToGo;


@end
