//
//  main.m
//  Story Uploader
//
//  Created by User543 on 19.07.17.
//  Copyright © 2017 User543. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
