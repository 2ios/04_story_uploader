//
//  StoreCoinsTableViewController.swift
//  Story Uploader
//
//  Created by User543 on 17.07.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import StoreKit

class StoreCoinsTableViewController: UITableViewController {

    var alert: UIAlertController? = nil
    var watchVideo = [String]()
    
    @IBAction func cancelBarButton(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var coinsLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NotificationCenter.default.addObserver(self, selector: #selector(startWaiting), name: NSNotification.Name(rawValue: "ShowWaiting"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(productLoaded), name: NSNotification.Name(rawValue: "SKProductsHaveLoaded"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(productLoadFailed), name: NSNotification.Name(rawValue: "SKProductsLoadFailed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(StoreCoinsTableViewController.catchNotification(notification:)), name: NSNotification.Name(rawValue: "CatchNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateCoinsLabel), name: NSNotification.Name(rawValue: "UpdateCoinsLabel"), object: nil)
        //Hide all lines
        self.tableView.tableFooterView = UIView(frame: CGRect(origin: CGPoint(), size: CGSize.init(width: UIScreen.main.bounds.size.width, height: 0.0001)))
        
        if StoreManager.shared.productsFromStore.count > 0 {
            self.watchVideo = ["Get 5 coins"]
        }
        
        //        SwiftyAd.shared.setup(
        //            withBannerID:    "",
        //            interstitialID:  "",
        //            rewardedVideoID: ""
        //        )
    }
    
    func updateCoinsLabel() {
        coinsLabel.text = "\(String(UserDefaults.standard.integer(forKey: "Wallet"))) coins"
    }
    
    func productLoaded() -> Void {
        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                        object: nil,
                                        userInfo: ["title" : "nil", "message" : ""])
        self.watchVideo = ["Get 5 coins"]
        self.tableView.reloadData()
    }

    func productLoadFailed() -> Void {
        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                        object: nil,
                                        userInfo:["title":"Sorry", "message":"Error. No products from the store!"])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        coinsLabel.text = "\(String(UserDefaults.standard.integer(forKey: "Wallet"))) coins"
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
//        let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
//        noDataLabel.text = ""
//        noDataLabel.textColor = UIColor.black
//        noDataLabel.textAlignment = .center
        self.tableView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "background.png"))
        
        showWaiting {
            if StoreManager.shared.productsFromStore.count == 0 {
                StoreManager.shared.setup()
            } else {
                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                object: nil,
                                                userInfo: ["title" : "nil", "message" : ""])
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Selector from cell button (Unlock button)
    
    func didTapCellButton(sender: UIButton){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowWaiting"), object: nil)
        let index = sender.tag
        let product = StoreManager.shared.productsFromStore[index]
        StoreManager.shared.buy(product: product)
    }
    
    func buyFromCell(index: Int){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowWaiting"), object: nil)
        let product = StoreManager.shared.productsFromStore[index]
        StoreManager.shared.buy(product: product)
    }
    
    func watchButtonTapped(_ sender: AnyObject) {
        
        print("Reclama activa")
        
        let test = self.storyboard?.instantiateViewController(withIdentifier: "Test")
        self.navigationController?.pushViewController(test!, animated: true)
    }

    //Number of sections
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    //Number of rows in section
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //Let's feed our table with the number of products
        if section == 0 {
            return watchVideo.count
        } else {
            return StoreManager.shared.productsFromStore.count
        }
    }
    
    //Cell for row
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FreeCoinsCell", for: indexPath) as! StoreCoinsCell
            cell.titleLabel.text = watchVideo[indexPath.row]
            cell.priceLabel.text = ""
            cell.buyButton.tag = indexPath.row
            cell.buyButton.addTarget(self, action: #selector(self.watchButtonTapped(_:)), for: UIControlEvents.touchUpInside)
            return cell
        } else {
            let product  = StoreManager.shared.productsFromStore[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "StoreCoinsCell", for: indexPath) as! StoreCoinsCell
            
            cell.titleLabel.text = product.localizedTitle
            
            //You should always use NumberFormatter for the price in order to show the correct price currency
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.locale = product.priceLocale
            
            cell.priceLabel.text = formatter.string(from: product.price)
            cell.buyButton.tag = indexPath.row
            cell.buyButton.addTarget(self, action: #selector(self.didTapCellButton(sender:)), for: UIControlEvents.touchUpInside)
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            watchButtonTapped(indexPath.section as AnyObject)
        } else {
            buyFromCell(index: indexPath.row)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func startWaiting() -> Void {
        showWaiting(completion: nil)
    }
    
    func showWaiting(completion: (() -> Void)? = nil) -> Void {
        if alert == nil {
            alert = UIAlertController(title: "", message: "Please wait\n\n\n", preferredStyle: .alert)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            spinner.center = CGPoint(x: 135.0, y: 65.5)
            spinner.color = UIColor.black
            spinner.startAnimating()
            alert?.view.addSubview(spinner)
        }
        let topVC = self.getCurrentViewController(self)!
        topVC.present(alert!, animated: true, completion: completion)
    }
}
