//
//  StoreCoinsCell.swift
//  Story Uploader
//
//  Created by User543 on 17.07.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import StoreKit

class StoreCoinsCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var buyButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
