//
//  MKStoreManager.m
//
//  Created by Mugunth Kumar on 17-Oct-09.
//  Copyright 2009 Mugunth Kumar. All rights reserved.
//  mugunthkumar.com
//

#import "MKStoreManager.h"


@implementation MKStoreManager

@synthesize purchasableObjects;
@synthesize storeObserver;
@synthesize delegate;

BOOL featureAPurchased;
BOOL featureBPurchased;
BOOL featureCPurchased;

static MKStoreManager* _sharedStoreManager; // self

+ (id)sharedManager {
    @synchronized(self) {
        
        if (_sharedStoreManager == nil) {
            
            _sharedStoreManager = [[self alloc] init]; // assignment not done here
            _sharedStoreManager.purchasableObjects = [[NSMutableArray alloc] init];
          
             _sharedStoreManager.storeObserver = [[MKStoreObserver alloc] init];
            [[SKPaymentQueue defaultQueue] addTransactionObserver:_sharedStoreManager.storeObserver];
                   }
        
    }
    return _sharedStoreManager;
}

- (id)init {
    if (self = [super init]) {
        [self loadPurchases];
    }
    return self;
}


- (void)dealloc {
    
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:_sharedStoreManager.storeObserver];

}




+ (BOOL) featurePurchasedWithName:(NSString*)purchasedName
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    featureAPurchased = [userDefaults boolForKey:purchasedName];
    return featureAPurchased;

}

#pragma mark Singleton Methods

+ (id)allocWithZone:(NSZone *)zone

{	
    @synchronized(self) {
		
        if (_sharedStoreManager == nil) {
			
            _sharedStoreManager = [super allocWithZone:zone];			
            return _sharedStoreManager;  // assignment and return on first allocation
        }
    }
	
    return nil; //on subsequent allocation attempts return nil	
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;	
}




- (void)productsRequest:(SKProductsRequest *)request
     didReceiveResponse:(SKProductsResponse *)response
{
    [purchasableObjects removeAllObjects];
	[purchasableObjects addObjectsFromArray:response.products];
	if ([purchasableObjects count]>0)
	{
		
		SKProduct *product = [purchasableObjects objectAtIndex:0];
        
        
        [self purchase:product];
		NSLog(@"Feature: %@, Cost: %f, ID: %@",[product localizedTitle],
			  [[product price] doubleValue], [product productIdentifier]);
        
      
            [[NSUserDefaults standardUserDefaults] setFloat:[[product price] doubleValue]  forKey:[product productIdentifier]];

	}
	
}





-(void)writeDateOnPurchasable:(NSString*)string
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    _arrayCurrentPurchased=[(NSMutableArray*)[userDefaults objectForKey:@"CurrentPurchased"] mutableCopy];
    if (!_arrayCurrentPurchased) {
        _arrayCurrentPurchased=[NSMutableArray new];
    }
     if (![_arrayCurrentPurchased containsObject:string]) {
         [_arrayCurrentPurchased addObject:string];
     }
}


- (void) buyFeature:(NSString*) featureId
{
	if ([SKPaymentQueue canMakePayments])
	{
        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:featureId]];
        productsRequest.delegate = self;
        [productsRequest start];
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"You are not authorized to purchase from AppStore"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
	}
}

-(void)paymentCanceled
{
	if([delegate respondsToSelector:@selector(failedPaymentTransaction)])
		[delegate failedPaymentTransaction];
 
}
- (IBAction)purchase:(SKProduct *)product{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}
- (void) failedTransaction: (SKPaymentTransaction *)transaction
{
    
	if([delegate respondsToSelector:@selector(failedPaymentTransaction)])
		[delegate failedPaymentTransaction];
  
	NSString *messageToBeShown = [NSString stringWithFormat:@"Reason: %@, You can try: %@", [transaction.error localizedFailureReason], [transaction.error localizedRecoverySuggestion]];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to complete your purchase" message:messageToBeShown
												   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];
}


-(void)provideContent: (NSString*) productIdentifier   transaction: (SKPaymentTransaction *)transaction
{

    if([delegate respondsToSelector:@selector(productPurchasedWithID: receipt: transaction:)])
    {
        NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
        NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
        [delegate productPurchasedWithID:productIdentifier receipt:receipt transaction:transaction];
    }
    [self updatePurchases];
}

-(void) provideContent: (NSString*) productIdentifier
{

}


-(void)finishRestored
{
    
    if ([delegate respondsToSelector:@selector(finishRestoredOrBuy)])
        [delegate finishRestoredOrBuy];
}



-(void)restore{
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    
}

-(void) loadPurchases
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    _arrayCurrentPurchased=[(NSMutableArray*)[userDefaults objectForKey:@"CurrentPurchased"] mutableCopy];
    if (!_arrayCurrentPurchased) {
        _arrayCurrentPurchased=[NSMutableArray new];
    }
}


-(void) updatePurchases
{
   NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    _arrayCurrentPurchased=[(NSMutableArray*)[userDefaults objectForKey:@"CurrentPurchased"] mutableCopy];
    if (!_arrayCurrentPurchased) {
        _arrayCurrentPurchased=[NSMutableArray new];
    }

}


-(void)unsubscribeAll
{

}

-(void)unsubscribeWithID:(NSString*)featureId
{

}


@end
