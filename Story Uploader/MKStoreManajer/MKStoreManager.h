//
//  StoreManager.h
//  MKSync
//
//  Created by Mugunth Kumar on 17-Oct-09.
//  Copyright 2009 MK Inc. All rights reserved.
//  mugunthkumar.com

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "MKStoreObserver.h"



#define kIAP_I_COINS_1_99   @"hocoins1"
#define kIAP_I_COINS_2_99   @"hocoins2"
#define kIAP_I_COINS_7_99   @"hocoins3"
#define kIAP_I_COINS_15_99  @"hocoins4"
#define kIAP_I_COINS_24_99  @"hocoins5"
#define kIAP_I_COINS_39_99  @"hocoins6"
#define kIAP_I_COINS_59_99  @"hocoins7"


@protocol MKStoreKitDelegate <NSObject>
@optional
- (void)productPurchasedWithID:(NSString*)featureId;
- (void)productPurchasedWithID:(NSString*)featureId receipt:(NSData *)receipt;
- (void)productPurchasedWithID:(NSString*)featureId receipt:(NSData *)receipt transaction: (SKPaymentTransaction *)transaction;
- (void)finishRestoredOrBuy;

- (void)failedPaymentTransaction;
@end

@interface MKStoreManager : NSObject<SKProductsRequestDelegate, SKPaymentTransactionObserver> {

	NSMutableArray *purchasableObjects;
	MKStoreObserver *storeObserver;	


    
	id<MKStoreKitDelegate> delegate;
}

@property (nonatomic, retain) id<MKStoreKitDelegate> delegate;
@property (nonatomic, retain) NSMutableArray *purchasableObjects;
@property (nonatomic, retain) NSMutableArray *arrayCurrentPurchased;
@property (nonatomic, retain) MKStoreObserver *storeObserver;

- (void) requestProductData;

- (void) buyFeature:(NSString*) featureId;

-(void)paymentCanceled;
-(void)finishRestored;
- (void) failedTransaction: (SKPaymentTransaction *)transaction;
-(void) provideContent: (NSString*) productIdentifier;

-(void)provideContent: (NSString*) productIdentifier   transaction: (SKPaymentTransaction *)transaction;

- (void) buyFeatureBuyIndex:(int)index;

+ (MKStoreManager*)sharedManager;

+ (BOOL) featurePurchasedWithName:(NSString*)purchasedName;


-(void) loadPurchases;
-(void) updatePurchases;

-(void)restore;

@end
