#import "MKStoreObserver.h"
#import "MKStoreManager.h"

@implementation MKStoreObserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
	for (SKPaymentTransaction *transaction in transactions)
	{
		switch (transaction.transactionState)
		{
			case SKPaymentTransactionStatePurchased:
				
                [self completeTransaction:transaction];
				
                break;
				
            case SKPaymentTransactionStateFailed:
				
                [self failedTransaction:transaction];
				
                break;
				
            case SKPaymentTransactionStateRestored:
				
                [self restoreTransaction:transaction];
				
            default:
				
                break;
		}			
	}
}




- (void) failedTransaction: (SKPaymentTransaction *)transaction
{
    
    if (transaction.error.code != SKErrorPaymentCancelled)		
    {
        NSLog(@"error {%@}",transaction.error);
        // Optionally, display an error here.		
    }	
	[[MKStoreManager sharedManager] paymentCanceled];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];	
}

- (void) completeTransaction: (SKPaymentTransaction *)transaction
{
    NSData *receipt1= transaction.transactionReceipt;
    NSString* transactionIdentifier= transaction.transactionIdentifier;
    [[MKStoreManager sharedManager] provideContent:transaction.payment.productIdentifier transaction:transaction];
    
//    [[MKStoreManager sharedManager] provideContent: transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
  
    
}

- (void) restoreTransaction: (SKPaymentTransaction *)transaction
{
    
    [[MKStoreManager sharedManager] provideContent: transaction.originalTransaction.payment.productIdentifier];	
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];	
}



- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error{
    [[MKStoreManager sharedManager] paymentCanceled];
    NSLog(@"avem eroare %@", error);
    

}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue{

    NSLog(@"avem eroare numaru 2 %@", queue.transactions);
    
    [[MKStoreManager sharedManager] paymentCanceled];
      [[MKStoreManager sharedManager] finishRestored];
}


@end
