//
//  ViewController.swift
//  Story Uploader
//
//  Created by User543 on 11.07.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import DKImagePickerController
import Photos
import KeychainSwift

class UploadViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var imageArray = [UIImage]()
    var selectedImage: UIImage!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var coinsView: UIView!
    
    @IBOutlet weak var coinsLabel: UILabel!
    @IBOutlet weak var coinsImage: UIImageView!
    
    
    @IBAction func addOlderPhotos(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getPhotoFromPickerController"), object: nil)
    }
    
    func addGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(goToBuy))
        coinsView.addGestureRecognizer(gesture)
    }
    
    func goToBuy() {
        performSegue(withIdentifier: "GoToStoreCoins", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        NotificationCenter.default.addObserver(self, selector: #selector(getPhotoFromPickerController), name: NSNotification.Name(rawValue: "getPhotoFromPickerController"), object: nil)
        self.addGesture()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        coinsLabel.text = String(UserDefaults.standard.integer(forKey: "Wallet"))
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var heightCollection: NSLayoutConstraint!

    override func viewDidLayoutSubviews() {
        if UI_USER_INTERFACE_IDIOM() == .pad {
            heightCollection.constant = 160
        } else {
            heightCollection.constant = 80
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UI_USER_INTERFACE_IDIOM() == .pad {
            return CGSize(width: 160, height: 160)
        } else {
            return CGSize(width: 80, height: 80)
        }
    }
    
    // MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let imageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        
        if indexPath.item == 0 {
            imageCell.imageCollectionView.image = UIImage(named: "open-gallery.png")
        } else {
            imageCell.imageCollectionView.image = imageArray[indexPath.item - 1]
        }
        return imageCell
    }
    
    // MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getPhotoFromPickerController"), object: nil)
        }
        
    }

    // MARK: GetPhotoFromPickerController
    
    func getPhotoFromPickerController() {
        let pickerController = DKImagePickerController()
        pickerController.showsCancelButton = true
        pickerController.maxSelectableCount = 1
        pickerController.didSelectAssets = { (assets: [DKAsset]) in
            print("didSelectAssets")
            
            let originalAsset = assets[0].originalAsset
            getImage(asset: originalAsset!)
            
            let count = assets.count
            var result = UserDefaults.standard.integer(forKey: "Wallet")
            if result - count * 300 < 0 {
                let alert = UIAlertController(title: "Sorry!",
                                              message:"You have insufficient coins",
                    preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Buy", style: UIAlertActionStyle.default, handler: { (action) in
                    self.goToBuy()
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                let alertImagesFilters = UIAlertController(title: "Good choice!", message: "Do you want add some effects to your photo ?", preferredStyle: UIAlertControllerStyle.alert)
                alertImagesFilters.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (action) in
                    self.performSegue(withIdentifier: "ToImagesFilters", sender: nil)
                }))
                alertImagesFilters.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: {
                    (action) in
                    let alert = UIAlertController(title: "Attention!",
                                                  message:"It will cost you \(count*300) coins",
                        preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                        result -= count * 300
                        UserDefaults.standard.set(result, forKey: "Wallet")
                        UserDefaults.standard.synchronize()
                        let keychain = KeychainSwift()
                        print(String(result))
                        keychain.set(String(result), forKey: "Wallet")
                        keychain.synchronizable = true
                        print(keychain.get("Wallet")!)
                        
                        self.coinsLabel.text = String(result)
                        for asset in assets {
                            let changeAsset = asset.originalAsset
                            if self.imageArray.count < 3 {
                                self.imageArray.insert(getAssetThumbnail(asset: changeAsset!), at: 0)
                            } else {
                                self.imageArray.removeLast()
                                self.imageArray.insert(getAssetThumbnail(asset: changeAsset!), at: 0)
                            }
                            
                            PHPhotoLibrary.shared().performChanges({
                                let request = PHAssetChangeRequest(for: changeAsset!)
                                request.creationDate = Date()
                            }, completionHandler: { (success: Bool, error: Error?) in
                                if success {
                                    print("Finished updating asset.")
                                } else {
                                    print("Error. \(error.debugDescription)")
                                }
                                DispatchQueue.main.async {
                                    self.collectionView.reloadData()
                                }
                            })
                        }
                        self.goToImportSuccessfullyViewController()
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }))
                self.present(alertImagesFilters, animated: true, completion: nil)
            }
        }
        
        func getImage(asset: PHAsset) {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .default, options: option) { (image, info) in
                self.selectedImage = image
            }
        }
        
        func getAssetThumbnail(asset: PHAsset) -> UIImage {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            var thumbnail = UIImage()
            option.isSynchronous = true
            
            let layoutAttr = collectionView.layoutAttributesForItem(at: IndexPath(item: 0, section: 0))
            let sizeThumbnail = layoutAttr?.size
            manager.requestImage(for: asset, targetSize: sizeThumbnail!, contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
                thumbnail = result!
            })
            return thumbnail
        }
        
        self.present(pickerController, animated: true) {}
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToImagesFilters" {
            let imagesFiltersVC: FilteredImageViewController = segue.destination as! FilteredImageViewController
            imagesFiltersVC.selectedImage = selectedImage
        }
    }
    
    func goToImportSuccessfullyViewController() {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "ImportSuccessfullyViewController")
//        controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
//        self.addChildViewController(controller)
//        self.view.addSubview(controller.view)
        
        let alert = UIAlertController(title: "Congratulations!",
                                      message:"Import successfully",
                                      preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Back", style: UIAlertActionStyle.default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Go to Instagram", style: UIAlertActionStyle.cancel, handler: { (action) in
            self.goToInstagram()
    }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func goToInstagram() {
        let instagramHooks = "instagram://app"
        let instagramUrl = URL.init(string: instagramHooks)!
        if UIApplication.shared.canOpenURL(instagramUrl as URL)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(instagramUrl, options: [:])
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(instagramUrl)
            }
        } else {
            let message = "Please install Instagram App first from App Store."
            let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                if let url = URL(string: "itms-apps://search.itunes.apple.com/WebObjects/MZSearch.woa/wa/search?media=software&term=instagram") {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:])
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            })
            alert.addAction(action)
            let topVC = self.getCurrentViewController(self)!
            topVC.present(alert, animated: true, completion: nil)
        }
    }
}

extension UIViewController {
    func getCurrentViewController(_ vc: UIViewController) -> UIViewController? {
        if let pvc = vc.presentedViewController {
            return getCurrentViewController(pvc)
        }
        else if let svc = vc as? UISplitViewController, svc.viewControllers.count > 0 {
            return getCurrentViewController(svc.viewControllers.last!)
        }
        else if let nc = vc as? UINavigationController, nc.viewControllers.count > 0 {
            return getCurrentViewController(nc.topViewController!)
        }
        else if let tbc = vc as? UITabBarController {
            if let svc = tbc.selectedViewController {
                return getCurrentViewController(svc)
            }
        }
        return vc
    }
    
    func catchNotification(notification:Notification) -> Void {
        guard let userInfo = notification.userInfo,
            let title = userInfo["title"] as? String,
            let message  = userInfo["message"] as? String else {
                print("No userInfo found in notification")
                return
        }
        var topVC = self.getCurrentViewController(self)!
        if topVC .isKind(of: UIAlertController.self) {
            topVC.dismiss(animated: true, completion: {
                let alert = UIAlertController(title: "\(title)",
                    message:"\(message)",
                    preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                topVC = self.getCurrentViewController(self)!
                if title != "nil" {
                    topVC.present(alert, animated: true, completion: nil)
                }
            })
        }
    }
}
