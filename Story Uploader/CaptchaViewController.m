//
//  CaptchaViewController.m
//  LikeHelper
//
//  Created by Motipan Serj on 12/16/16.
//  Copyright © 2016 Piotr Arabadji. All rights reserved.
//

#import "CaptchaViewController.h"

@interface CaptchaViewController ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIView *viewLoading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTopBar;
- (IBAction)cancelAction:(id)sender;

@end

@implementation CaptchaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewLoading.hidden=NO;
    if (self.cookies!=nil)
    {
    NSArray *array=[self.cookies componentsSeparatedByString:@";"];
    NSString *cfsrtd=@"";
    if (array.count>0)
    {
        NSArray *cfrstArr=[[array objectAtIndex:0] componentsSeparatedByString:@"="];
        if (cfrstArr.count>0)
        {
            cfsrtd=[cfrstArr objectAtIndex:1];
        }
    }
    NSURL *url = [NSURL URLWithString:self.link];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request setValue:@"1" forHTTPHeaderField:@"x-instagram-ajax"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"x-requested-with"];
    [request setValue:@"https:www.instagram.com" forHTTPHeaderField:@"origin"];
 
    [request setValue:@"*/*" forHTTPHeaderField:@"Accept"];
    [request setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36" forHTTPHeaderField:@"User-Agent"];
    [request setValue:@"gzip, deflate, br" forHTTPHeaderField:@"Accept-Encoding"];
    [request setValue:@"en-US,en;q=0.8" forHTTPHeaderField:@"Accept-Language"];
    [request setValue:cfsrtd forHTTPHeaderField:@"x-csrftoken"];
    [request setValue:@"www.instagram.com" forHTTPHeaderField:@"Host"];
    [request setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
    [request setValue:@"0" forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setValue:cfsrtd forHTTPHeaderField:@"x-csrftoken"];
    
    [request setValue:self.cookies forHTTPHeaderField:@"Cookie"];
        _webView.delegate=self;
        [self.webView loadRequest:request];
        
   

    }
    else
    {
        NSString *currentUser=[[NSUserDefaults standardUserDefaults] objectForKey:@"currentUser"];
        NSMutableDictionary *usersDictionary=[[[NSUserDefaults standardUserDefaults] objectForKey:@"usersInstagrammDictionary"] mutableCopy] ;
        NSDictionary* tempDic=[usersDictionary objectForKey:currentUser];
        if (![self.link containsString:@"https://www.instagram.com"])
            self.link=[NSString stringWithFormat:@"https://www.instagram.com%@",self.link];
        NSURL *url = [NSURL URLWithString:self.link];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        
        [request setValue:@"1" forHTTPHeaderField:@"x-instagram-ajax"];
        [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"x-requested-with"];
        [request setValue:@"https:www.instagram.com" forHTTPHeaderField:@"origin"];
        
        [request setValue:@"*/*" forHTTPHeaderField:@"Accept"];
        [request setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36" forHTTPHeaderField:@"User-Agent"];
        [request setValue:@"gzip, deflate, br" forHTTPHeaderField:@"Accept-Encoding"];
        [request setValue:@"en-US,en;q=0.8" forHTTPHeaderField:@"Accept-Language"];
        [request setValue:[(NSDictionary*)tempDic objectForKey:@"csrftoken"] forHTTPHeaderField:@"x-csrftoken"];
        [request setValue:@"www.instagram.com" forHTTPHeaderField:@"Host"];
        [request setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
        [request setValue:@"0" forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        
        
        NSString *str=[NSString stringWithFormat:@"mid=%@; csrftoken=%@;",[tempDic objectForKey:@"mid"],[tempDic objectForKey:@"csrftoken"]];
        
        [request setValue:str forHTTPHeaderField:@"Cookie"];
        
        _webView.delegate=self;
        [self.webView loadRequest:request];
        
        
    }
    if (![self.link containsString:@"challenge"])
    {
        self.heightTopBar.constant=67.0;
    }
    else
    {
        self.heightTopBar.constant=67.0;
    }
    
    
    
    [self.view layoutIfNeeded];
       // Do any additional setup after loading the view from its nib.
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    self.viewLoading.hidden=YES;
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSMutableURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *string=request.URL.absoluteString;
    NSLog(@"jopa %@", string);
    if ([string containsString:@"instagram://checkpoint/dismiss"] && ![self.link containsString:@"challenge"])
    {
          [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissModalView" object:@"1"];
        [self closeThisView];
    }
     if ([string containsString:@"instagram://checkpoint/dismiss"] && [self.link containsString:@"challenge"])
     {
         return NO;
     }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)closeThisView
{
  
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelAction:(id)sender {

    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Are you sure?"
                                 message:@"You will have to use another account."
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissModalView" object:@"0"];
                                [self closeThisView];
                                    //Handle your yes please button action here
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"NO"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}
@end
