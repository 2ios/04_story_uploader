//
//  HelpVideoViewController.swift
//  Story Uploader
//
//  Created by User543 on 25.07.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class HelpVideoViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let url = Bundle.main.url(forResource: "HelpVideo", withExtension: "mov")
        let player = AVPlayer(url: url!) 
        let playerController = AVPlayerViewController()
        playerController.player = player
        playerController.view.frame = self.view.frame
        self.addChildViewController(playerController)
        self.view.addSubview(playerController.view)
        player.play()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
