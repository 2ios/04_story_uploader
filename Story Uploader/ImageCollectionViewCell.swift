//
//  ImageCollectionViewCell.swift
//  Story Uploader
//
//  Created by User543 on 12.07.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageCollectionView: UIImageView!
}
