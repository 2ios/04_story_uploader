//
//  TestViewController.swift
//  Story Uploader
//
//  Created by User543 on 19.07.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import AppLovinSDK

class TestViewController: UIViewController, ALAdLoadDelegate, ALAdDisplayDelegate, ALAdVideoPlaybackDelegate {
    
    var interstitial: ALInterstitialAd?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if ALInterstitialAd.isReadyForDisplay()
        {
            // Make it a class property so it doesn't get dealloc'd by ARC
            
            self.interstitial = ALInterstitialAd(sdk: ALSdk.shared()!) // You can also explicitly enter sdk key so: ALSdk.sharedWithKey("YOUR KEY") (i.e. if you have multiple keys)
            
            // Optional: Assign delegates.
            self.interstitial?.adLoadDelegate = self;
            self.interstitial?.adDisplayDelegate = self;
            self.interstitial?.adVideoPlaybackDelegate = self; // This will only ever be used if you have video ads enabled.
            
            /*
             NOTE: We recommend the use of placements (AFTER creating them in your dashboard):
             
             self.interstitial?.showOverPlacement("SINGLE_INSTANCE_SCREEN")
             
             To learn more about placements, check out https://applovin.com/integration#iosPlacementsIntegration
             */
            
            self.interstitial?.show()
            print("Interstitial Shown")
        }
        else
        {
            // Ideally, the SDK preloads ads when you initialize it in application:didFinishLaunchingWithOptions: of the app delegate
            // you can manually load an ad as demonstrated in the ALDemoInterstitialManualLoadingViewController class
            print("Interstitial not ready for display.\nPlease check SDK key or internet connection.")
            let alert = UIAlertController(title:"We are sorry but now there is no more free picks. You can try a little bit later.", message: nil, preferredStyle: UIAlertControllerStyle.alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            })
            
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Ad Load Delegate
    
    func adService(_ adService: ALAdService, didLoad ad: ALAd)
    {
        print("Interstitial Loaded")
    }
    
    func adService(_ adService: ALAdService, didFailToLoadAdWithError code: Int32)
    {
        // Look at ALErrorCodes.h for list of error codes
        print("Interstitial failed to load with error code \(code)")
        let alert = UIAlertController(title:"We are sorry but now there is no more free picks. You can try a little bit later.", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        })
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: Ad Display Delegate
    
    func ad(_ ad: ALAd, wasDisplayedIn view: UIView)
    {
        print("Interstitial Displayed")
    }
    
    func ad(_ ad: ALAd, wasHiddenIn view: UIView)
    {
        print("Interstitial Dismissed")
        dismiss(animated: true, completion: nil)
    }
    
    func ad(_ ad: ALAd, wasClickedIn view: UIView)
    {
        print("Interstitial Clicked")
    }
    
    // MARK: Ad Video Playback Delegate
    
    func videoPlaybackBegan(in ad: ALAd)
    {
        print("Video Started")
    }
    
    func videoPlaybackEnded(in ad: ALAd, atPlaybackPercent percentPlayed: NSNumber, fullyWatched wasFullyWatched: Bool)
    {
        print("Video Ended")
        if let vc: InstagramLikeViewController = self.presentingViewController as? InstagramLikeViewController {
            vc.get5coins()
        }
        var result = UserDefaults.standard.integer(forKey: "Wallet")
        result = result + 5
        UserDefaults.standard.set(result, forKey: "Wallet")
        UserDefaults.standard.synchronize()
    }
}

